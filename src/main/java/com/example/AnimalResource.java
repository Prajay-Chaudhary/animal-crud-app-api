package com.example;
import io.quarkus.security.Authenticated;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;


import java.net.URI;
import java.util.List;

@Path("/animals")
public class AnimalResource {

    // Get all animals
    @GET
    @Authenticated
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        List<Animal> animals = Animal.listAll();
        return Response.ok(animals).build();
    }

    // Get by id
    @GET
    @Authenticated
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getById(@PathParam("id") Long id) {
        return Animal.findByIdOptional(id)
                .map(animal -> Response.ok(animal).build())
                .orElse(Response.status(Response.Status.NOT_FOUND).build());
    }

    // Search animals by name
    @GET
    @Path("/searchByName")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchByName(@QueryParam("name") String name) {
        List<Animal> results = Animal.find("name", name).list();
        if (results.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(results).build();
        }
    }


    // Create an animal
    @POST
    @Authenticated
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response createAnimal(Animal animal) {
        animal.persist();
        URI uri = URI.create("/animals/" + animal.id);
        return Response.created(uri).build();
    }

    // To update the existing animal
    @PUT
    @Authenticated
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response updateAnimal(@PathParam("id") Long id, Animal updatedAnimal) {
        Animal existingAnimal = Animal.findById(id);
        if (existingAnimal != null) {
            existingAnimal.name = updatedAnimal.name;
            existingAnimal.type = updatedAnimal.type;
            existingAnimal.persist();
            return Response.ok(existingAnimal).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }


    // Delete
    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    @Authenticated
    public Response deleteAnimal(@PathParam("id") Long id) {
        return Animal.findByIdOptional(id)
                .map(animal -> {
                    animal.delete();
                    return Response.ok().build();
                })
                .orElse(Response.status(Response.Status.NOT_FOUND).build());
    }

}
