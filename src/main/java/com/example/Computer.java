package com.example;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import jakarta.persistence.Entity;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;


@Entity
@Indexed
public class Computer extends PanacheEntity {
    public String name;
    public String type;
    public double price;
}
