package com.example;

import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.net.URI;
import java.util.List;

@Path("/computers")
public class ComputerResource {

    // Get all computers
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        List<Computer> computers = Computer.listAll();
        return Response.ok(computers).build();
    }

    // Get by id
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getById(@PathParam("id") Long id) {
        return Computer.findByIdOptional(id)
                .map(computer -> Response.ok(computer).build())
                .orElse(Response.status(Response.Status.NOT_FOUND).build());
    }

    // Search computers by name
    @GET
    @Path("/searchByName")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchByName(@QueryParam("name") String name) {
        List<Computer> results = Computer.find("name", name).list();
        if (results.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(results).build();
        }
    }


    // Create a computer
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response createComputer(Computer computer) {
        computer.persist();
        URI uri = URI.create("/computers/" + computer.id);
        return Response.created(uri).build();
    }

    // Update the existing computer
    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response updateComputer(@PathParam("id") Long id, Computer updatedComputer) {
        Computer existingComputer = Computer.findById(id);
        if (existingComputer != null) {
            existingComputer.name = updatedComputer.name;
            existingComputer.type = updatedComputer.type;
            existingComputer.price = updatedComputer.price; // Include price update
            existingComputer.persist();
            return Response.ok(existingComputer).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    // Delete
    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response deleteComputer(@PathParam("id") Long id) {
        return Computer.findByIdOptional(id)
                .map(computer -> {
                    computer.delete();
                    return Response.ok().build();
                })
                .orElse(Response.status(Response.Status.NOT_FOUND).build());
    }
}
